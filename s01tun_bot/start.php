<?php

ini_set('display_errors',0);
error_reporting(E_ALL);
ini_set('zlib.output_compression','Off');
// ini_set('memory_limit','512M');
ini_set('max_execution_time',600);

define('C_DIR',__DIR__);

foreach(['lang','con','fun_str','fun_send','fun_db','run'] as $file){
	require(C_DIR.'/inc/'.$file.'.php');
}
