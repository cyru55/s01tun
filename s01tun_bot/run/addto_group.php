<?php

// while add bot into a group
if(
	isset($json['my_chat_member'])&&
	isset($json['my_chat_member']['new_chat_member'])&&
	isset($json['my_chat_member']['new_chat_member']['status'])&&
	$json['my_chat_member']['new_chat_member']['status']!='left'
){

	// hey admin, somebody try add me in a new group
	$pure_name=rtrim($json['my_chat_member']['from']['first_name'].' '.($json['my_chat_member']['from']['last_name']??''),' ');
	$send[]=[
		'cmd'=>'sendMessage',
		'arr'=>[
			'chat_id'=>C_G_ADMIN[0],
			'text'=>
				'@'.$json['my_chat_member']['from']['username'].' | '.$pure_name.' | tg://user?id='.$json['my_chat_member']['from']['id'].PHP_EOL
				.'try add me in a '.$json['my_chat_member']['chat']['type'].PHP_EOL
				.$json['my_chat_member']['chat']['id'].PHP_EOL
				.($json['my_chat_member']['chat']['username']??'').PHP_EOL
				.$json['my_chat_member']['chat']['title']
		]
	];

	if(in_array($json['my_chat_member']['chat']['id'],C_ALLOWED_G)){
		if($json['my_chat_member']['new_chat_member']['status']!='administrator'){
			$send[]=[
				'cmd'=>'sendMessage',
				'arr'=>[
					'chat_id'=>$json['my_chat_member']['chat']['id'],
					'text'=>gLng('fa/add_mine_allowed_notadmin'),
				]
			];
			$send[]=[
				'cmd'=>'leaveChat',
				'arr'=>[
					'chat_id'=>$json['my_chat_member']['chat']['id'],
				]
			];
		}else{
			$no_perm=
				$json['my_chat_member']['new_chat_member']['can_delete_messages']==false||
				$json['my_chat_member']['new_chat_member']['can_restrict_members']==false;
			$send[]=[
				'cmd'=>'sendMessage',
				'arr'=>[
					'chat_id'=>$json['my_chat_member']['chat']['id'],
					'parse_mode'=>'MarkdownV2',
					'text'=>
						sprintf(
							gLng('fa/add_mine_allowed'),
							mde($name),
							str($json['my_chat_member']['from']['id'])
						)
						.gLng($no_perm?'fa/add_mine_allowed_noperm':'fa/add_mine_allowed_permok'),
				]
			];
		}
	}else{
		$send[]=[
			'cmd'=>'sendMessage',
			'arr'=>[
				'chat_id'=>$json['my_chat_member']['chat']['id'],
				'text'=>gLng('fa/add_mine_not_allowed'),
			]
		];
		$send[]=[
			'cmd'=>'leaveChat',
			'arr'=>[
				'chat_id'=>$json['my_chat_member']['chat']['id'],
			]
		];
	}

}
