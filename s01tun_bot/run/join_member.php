<?php

// while add bot into a group
if(
	isset($json['chat_member'])&&
	$json['chat_member']['new_chat_member']['status']=='member'&&
	$json['chat_member']['chat']['id']==C_ALLOWED_G[0]
){

	// say welcome to someone join in our group
	$allowed_users=[// list who can join and stay
		'1328325756',// Bargahi
		'1995374444',// @Kaveh_MZ 𝓚𝓪𝓿𝓮𝓱 𝓜𝓩
		'585323621',// @Rezatanhaam ˙·٠•●♥️رضا...تنها♥️●•٠·˙
		'5283893458',// @xwerfa2 𝒆𝒓𝒇𝒂𝒏
		'335727166',// @rasol0098 Rasol
		'770284792',// ❰❰❰ 𝙺𝙴𝙽𝚉𝙾 ❱❱❱
		'1826312667',// @MrAfaz 𝐌𝐫 𝐀𝐅𝐀𝐙
		'6440393724',// @Unknown_c_137 ᵀᴴᴱ➣яιϲκ
		'5082270059',// @amiria703 Amiria
		'5538651943',// @Moham_MAX Sm²a²X
		'530831398',// @NemesisNightmare M.E#23
		'1937089882',// ▓▒░۩۩ 𝘼𝙡𝙞 ۩۩░▒▓
		'1816549539',// @SobhanCU - 410 Gone :)
		'1034287105',// @La3tKnight La3tKnight
		'1759571494',// @shgh8484 #shahram#
		'5477199808',// ᖇꫀຊꪖ @rezaAa1177
		'5054815870',// @Amirw_XD Amirw
		'2029679739',// @ITTQQ mahan, POLYBIUS
		'1676325099',// @SAbolfazlSH Abolfazl Hosseiny
		'1223729610',// @MahdiyarDev Mahdiyar
		'932528835',// @mosishon mosTafa
		'291816682',// @SepehrBrJ Sepehr
		'5810129660',// @Inverse_Gravity Inverse ʎʇᴉʌɐɹפ
		'387851811',// @sohrabbehdani Sohrab
		'1644534975',// @rentito Rentito
		'5592269633',// @SenatorEdmond Senator
		'5641106288',// @ImDanielN Daniel
		'705834428',// @DarkKnight_Communication ƊƛƦƘƘƝƖƓӇƬ
		'1146958478',// @TerNative ᴛᴇʀɴᴀᴛɪᴠᴇ
		'1260250040',// @mGh_1400 𝐉𝐀𝐕𝐀𝐃
		'1303340274',// @EV0L_ution Javad
		'1535034143',// @s0013 Sia(M)
		'634579889',// @strawberry_cheesecake Strawberry Cheesecake
		'574197099',// @ItIsMeSAEED SAEED
		'5952588277',// @ayhan_it_dev 𝑴𝑹 𝑨𝒚𝒉𝒂𝒏 🇹🇷 / 🇷🇺
	];

	if(in_array($json['chat_member']['from']['id'],$allowed_users)){

		$send[]=[
			'cmd'=>'sendMessage',
			'arr'=>[
				'chat_id'=>$json['chat_member']['chat']['id'],
				'text'=>gLng('fa/wlc_allowed'),
			]
		];

	}else{

		$send[]=[
			'cmd'=>'sendMessage',
			'arr'=>[
				'chat_id'=>$json['chat_member']['chat']['id'],
				'parse_mode'=>'MarkdownV2',
				'disable_web_page_preview'=>1,
				'text'=>sprintf(
					gLng('fa/wlc_not_allowed'),
					mde($name),
					str($json['chat_member']['from']['id'])
				)
			]
		];

		$send[]=[
			'delay'=>32,
			'cmd'=>'banChatMember',
			'arr'=>[
				'chat_id'=>$json['chat_member']['chat']['id'],
				'user_id'=>$json['chat_member']['from']['id'],
				'until_date'=>time()+90, //90-32
			]
		];

		/*$send[]=[
			'delay'=>1,
			'cmd'=>'unbanChatMember',
			'arr'=>[
				'chat_id'=>$json['chat_member']['chat']['id'],
				'user_id'=>$json['chat_member']['from']['id'],
				'only_if_banned'=>1,
			]
		];*/

	}

}
