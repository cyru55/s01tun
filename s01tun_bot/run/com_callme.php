<?php

// response if users call my name
if(
	strlen($text)&&
	3==$sent_count&&
	!$bad_word&&
	preg_match(gLng('_/com/callme/rgx'),$text)
){

	$send[]=[
		'cmd'=>'sendMessage',
		'arr'=>[
			'chat_id'=>$json['message']['chat']['id'],
			'reply_to_message_id'=>$json['message']['message_id'],
			'text'=>gLng('_/com/callme/resp'),
		]
	];

}
