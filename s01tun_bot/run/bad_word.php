<?php

// detect and remove bad words
if(
	strlen($text4detect)
	&&!in_array($msg['from']['id'],array_merge(C_G_ADMIN,C_BW_WHITE))
	//$msg['chat']['id']!=C_ALLOWED_G[1]
){

	if(
		preg_match('/('.implode('|',C_LNG['fa']['badwords_r']).')/u',$text4detect)||
		preg_match('/\b('.implode('|',C_LNG['fa']['badwords_r_b']).')\b/u',$text4detect)||
		preg_match('/('.arr_rgx_str(C_LNG['fa']['badwords']).')/u',$text4detect)||
		preg_match('/\b('.arr_rgx_str(C_LNG['fa']['badwords_b']).')\b/u',$text4detect)
	){

		$bad_word=1;
		$final=preg_replace('/('.implode('|',C_LNG['fa']['badwords_r']).')/u','……',$text4detect);
		$final=preg_replace('/\b('.implode('|',C_LNG['fa']['badwords_r_b']).')\b/u','……',$final);
		$final=preg_replace('/('.arr_rgx_str(C_LNG['fa']['badwords']).')/u','……',$final);
		$final=preg_replace('/\b('.arr_rgx_str(C_LNG['fa']['badwords_b']).')\b/u','……',$final);

		$mention=isset($msg['sender_chat'])?
			'‏🔥 '.$name:
			'‏⚠️ ['.mde($name).'](tg://user?id='.$msg['from']['id'].')';

		$send[]=[
			'cmd'=>'deleteMessage',
			'arr'=>[
				'chat_id'=>$msg['chat']['id'],
				'message_id'=>$msg['message_id'],
			]
		];

		$send[]=[
			'cmd'=>'sendMessage',
			'arr'=>[
				'chat_id'=>$msg['chat']['id'],
				'reply_to_message_id'=>$msg['reply_to_message']['message_id']??0,
				'parse_mode'=>'MarkdownV2',
				'text'=>$mention.gLng('fa/badword_said').mde($final),
			]
		];

	}

}
