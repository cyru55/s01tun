<?php

// detect and remove bad words
if(
	isset($json['message']['reply_to_message']['message_id'])&&
	isset($json['message']['from']['id'])&&
	in_array($json['message']['from']['id'],C_G_ADMIN)&&
	strlen($text)>0&&
	mb_strlen($text)<30&&
	preg_match(gLng('_/com/mute/rgx'),$text)
){

	$send[]=[
		'cmd'=>'deleteMessage',
		'arr'=>[
			'chat_id'=>$json['message']['chat']['id'],
			'message_id'=>$json['message']['message_id'],
		]
	];

	if(!in_array($json['message']['reply_to_message']['from']['id'],C_G_ADMIN)){

		$sent_count=0;
		$reply_name=$json['message']['reply_to_message']['from']['first_name'].' '.($json['message']['reply_to_message']['from']['last_name']??'');
		$reply_name=safe(trim($reply_name),1);
		$reply_name=mb_strlen($reply_name)>2?$reply_name:gLng('_/name_noname');
		$mention='[⚠️'.mde($reply_name).'](tg://user?id='.($json['message']['reply_to_message']['from']['id']).') ';

		$send[]=[
			'cmd'=>'restrictChatMember',
			'arr'=>[
				'chat_id'=>$json['message']['chat']['id'],
				'user_id'=>$json['message']['reply_to_message']['from']['id'],
				'permissions'=>jsone([
					'can_send_messages'=>false,
					'can_send_media_messages'=>false,
					'can_send_polls'=>false,
					'can_send_other_messages'=>false,
					'can_add_web_page_previews'=>false,
					'can_change_info'=>false,
					'can_invite_users'=>false,
					'can_pin_messages'=>false,
				]),
				'until_date'=>time()+1800,
			]
		];

		$send[]=[
			'cmd'=>'sendMessage',
			'arr'=>[
				'chat_id'=>$json['message']['chat']['id'],
				'text'=>$mention.gLng('_/com/mute/resp'),
				'parse_mode'=>'MarkdownV2',
			]
		];

	}else{ // do not restrict admin users
		$send[]=[
			'cmd'=>'sendMessage',
			'arr'=>[
				'chat_id'=>$json['message']['chat']['id'],
				'text'=>gLng('fa/no_admin_restrict'),
			]
		];
	}

}
