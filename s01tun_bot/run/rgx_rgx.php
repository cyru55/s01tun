<?php

// replace regex found string with matched regex strings
if(
	isset($json['message']['from']['id'])&&
	in_array($json['message']['from']['id'],C_G_ADMIN)&&
	strlen($text)
	//&&!$bad_word
){

	foreach(C_LNG['fa']['rgx_rgx'] as $k=>$v){
		if(preg_match($k,$text,$match)){

			--$sent_count;
			$text=$v;

			while(preg_match('/\$(\d+)/',$text,$find)){
				$text=str_replace('$'.$find[1],$match[$find[1]],$text);
			}

			$send[]=[
				'cmd'=>'deleteMessage',
				'arr'=>[
					'chat_id'=>$json['message']['chat']['id'],
					'message_id'=>$json['message']['message_id'],
				]
			];

			$send[]=[
				'cmd'=>'sendMessage',
				'arr'=>[
					'chat_id'=>$json['message']['chat']['id'],
					'reply_to_message_id'=>(
						isset($json['message']['reply_to_message'])?$json['message']['reply_to_message']['message_id']:0
					),
					'text'=>$text,
				]
			];

		}
	}

}
