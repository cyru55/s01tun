<?php

// remove channel messages and ban permanently
if(
	isset($json['message']['from']['username'])&&
	$json['message']['from']['username']=='Channel_Bot'
){

	$send[]=[
		'cmd'=>'deleteMessage',
		'arr'=>[
			'chat_id'=>$json['message']['chat']['id'],
			'message_id'=>$json['message']['message_id'],
		]
	];

	$send[]=[
		'cmd'=>'banChatSenderChat',
		'arr'=>[
			'chat_id'=>$json['message']['chat']['id'],
			'sender_chat_id'=>$json['message']['sender_chat']['id'],
		]
	];

}
