<?php

// response if users call my name
if(
	isset($json['message']['reply_to_message']['message_id'])&&
	isset($json['message']['from']['id'])&&
	in_array($json['message']['from']['id'],C_G_ADMIN)&&
	strlen($text)>0&&
	mb_strlen($text)<30&&
	preg_match(gLng('_/com/jaish/rgx'),$text)
){

	$send[]=[
		'cmd'=>'deleteMessage',
		'arr'=>[
			'chat_id'=>$json['message']['chat']['id'],
			'message_id'=>$json['message']['message_id'],
		]
	];

	$send[]=[
		'cmd'=>'sendMessage',
		'arr'=>[
			'chat_id'=>$json['message']['chat']['id'],
			'reply_to_message_id'=>$json['message']['reply_to_message']['message_id'],
			'text'=>gLng('_/com/jaish/resp'),
		]
	];

}
