<?php

function req(string $cmd,array $arr,$token=null){
	return @file_get_contents(
		'https://api.telegram.org/bot'.($token??C_TOKEN).'/'.$cmd,0,
		stream_context_create([
			'http'=>[
				'ignore_errors'=>true,
				'timeout'=>8,
				'header'=>"Content-Type: application/json",
				'content'=>jsone($arr)
			],
			'ssl'=>[
				'verify_peer'=>false,
				'verify_peer_name'=>false
			]
		])
	);
}

function send(string $cmd,array $arr){
	global $msg;
	return req($cmd,$arr+[
		'chat_id'=>$msg['chat']['id']??C_G_ADMIN[0],
	]);
}

function developer(string $str){
	send('sendMessage',[
		'chat_id'=>C_G_ADMIN[0],
		'text'=>'🔴 '.$str,
	]);
}
