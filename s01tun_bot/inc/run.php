<?php

$input_str=file_get_contents('php://input');
// error_log($str,3,C_PTH_ERR);

if(is_string($input_str)){
	$json=jsond($input_str,1);
	if(!empty($json)){

		$msg=$json['edited_message']??$json['message'];
		if(isset($msg['chat']['type'])&&$msg['chat']['type']=='private') exit;

		$from=$msg['from']??$json['chat_member']['from']??$json['my_chat_member']['from']??[];
		if(isset($from['first_name'])){
			$name=($msg['author_signature']??$from['first_name']).' '.($from['last_name']??'');
			$name=safe(trim($name),1);
			$name=mb_strlen($name)>2?$name:gLng('_/name_noname');
		}else $name='-no-valid-name-found-';

		if(isset($msg['text'])){
			$text=$msg['text'];
			$text4detect=preg_replace(
				'/[^'.
					'0-9a-zA-Z\s'.// english
					'\x{621}-\x{63A}\x{641}-\x{64A}\x{660}-\x{66A}'.// arabic
					'\x{67E}\x{686}\x{698}\x{6A9}\x{6AF}\x{6CC}\x{6F0}-\x{6F9}'.// persian
				']/u',
				'',
				$text,
			);
			$text4detect=str_replace(
				C_LNG['_']['str_conv_detect_from'],
				C_LNG['_']['str_conv_detect_to'],
				safe($text4detect,1)
			);
		}else $text=$text4detect='';

		$bad_word=0;
		$sent_count=3;
		$send=[];

		foreach (['ban_channel','addto_group','com_delmsg','mute','bad_word','rgx_rgx','rgx_arr','reply_onme','com_callme','join_member','com_jaish'] as $f){
			include(C_DIR_RUN.'/'.$f.'.php');
		}

		// send extra commands after immediately response
		if(count($send)){
			foreach($send as $i=>$msg){
				if(!isset($msg['delay'])){
					send($msg['cmd'],$msg['arr']);
					unset($send[$i]);
				}
			}
			foreach($send as $msg){
				sleep(intval($msg['delay']));
				send($msg['cmd'],$msg['arr']);
			}
		}

	}else developer('decoded payload data is empty');
}else developer('receive a '.$_SERVER['REQUEST_METHOD'].' call without any payload data');
