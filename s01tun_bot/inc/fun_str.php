<?php

function jsone(array|string $str){
	return empty($str)?
		'':json_encode($str,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE|JSON_INVALID_UTF8_IGNORE);
}

function jsond(string $str){
	return strlen($str)?
		json_decode($str,1,64,JSON_BIGINT_AS_STRING|JSON_INVALID_UTF8_IGNORE):[];
}

function str(int|float|string $str,$lf=1){
	if(is_float($str))
		return sprintf('%.'.$lf.'f',$str);
	if(is_int($str))
		return strval($str);
	if(is_string($str)){
		$str=preg_replace('/[\x0-\x8'.($lf?'\xB':'\xA').'-\x1F\x7F]/','',$str);
		$valid=mb_check_encoding($str,'UTF-8');
		return $valid?$str:'';
	}
	return '';
}

function safe(int|float|string $str,$how=1){
	$str=str($str);
	if(strlen($str)){
		if($how<1){// 0 valid utf8
			return preg_replace(
				'/[^'.
					'[\x00-\x7F]'.
					'|[\xC2-\xDF][\x80-\xBF]'.
					'|\xE0[\xA0-\xBF][\x80-\xBF]'.
					'|[\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}'.
					'|\xED[\x80-\x9F][\x80-\xBF]'.
					'|\xF0[\x90-\xBF][\x80-\xBF]{2}'.
					'|[\xF1-\xF3][\x80-\xBF]{3}'.
					'|\xF4[\x80-\x8F][\x80-\xBF]{2}'.
				']/',
				''
				,$str
			);
		}
		if($how<2){// 1 valid lang
			return preg_replace(
				'/[^'.
					'\x09\x0A\x20-\x7E'.// ascii
					'\x{391}-\x{3FF}'.// greek
					'\x{400}-\x{482}\x{48A}-\x{52F}'.// cyrillic
					'\x{531}-\x{556}\x{560}-\x{588}'.// armenian
					'\x{5D0}-\x{5F4}'.// hebrew
					'\x{621}-\x{63A}\x{640}-\x{656}\x{660}-\x{66A}'.// arabic
					'\x{6A9}\x{6AF}\x{686}\x{67E}\x{698}\x{6CC}\x{6F0}-\x{6F9}'.// persian
					'\x{985}-\x{9B9}'.// bengali
					'\x{E01}-\x{E39}\x{E4F}-\x{E5B}'.// thai
					'\x{1100}-\x{115E}'.// hangul
					'\x{3041}-\x{3093}\x{30A1}-\x{30F6}'.// japanese
					'\x{200c}'.// special zwnj,
				']/u',
				'',
				$str
			);
			//www.fileformat.info/info/unicode/block/hangul_syllables/utf8test.htm
			//memory.loc.gov/diglib/codetables/9.3.html
			//memory.loc.gov/diglib/codetables/9.1.html
			//stackoverflow.com/a/56314869
		}
		if($how<3){// 2 ascii
			return preg_replace('/[^\x20-\x7E]/','',$str);
		}
		if($how<4){// 3 alpha
			return preg_replace('/[^A-Za-z0-9]/','',$str);
		}
		if($how<5){// 4 float
			return doubleval(preg_replace('/[^-0-9\.]/','',$str));
		}
		if($how<6){// 5 digit
			return preg_replace('/[^0-9]/','',$str);
		}
		if($how<7){// 6 extract number
			return preg_match('/-?\d+/',$str,$match)?$match[0]:'';
		}
	}
	return '';
}

function mde(string $str,int $w=0){
	return addcslashes($str,
		$w<1?'\\_*[]()~`>#+-=|{}.!':
			($w<2?'\\`':// 1 in the code
				'\\)'// 2 in the parenthese
			)
	);
}

function gLng(string $key){
	$val='';
	$arr=explode('/',$key);
	while($path=@array_shift($arr)){// may array_shift on string in bas key usage
		$val=(
			is_array($val)?$val[$path]:C_LNG[$path]
		)??'-no-lng-key-';
	}
	if(is_array($val))
		return $val[rand(0,count($val)-1)];
	return $val;
}

function arr_rgx_str(array $arr){
	foreach($arr as &$word)
		$word=preg_quote($word,'/');
	return implode('|',$arr);
}
